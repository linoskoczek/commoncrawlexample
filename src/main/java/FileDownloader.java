import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class FileDownloader {
    private URL fileURL;
    private String fileName;
    private int retryNumber;

    public FileDownloader(String url, int retryNumber) throws MalformedURLException {
        this.fileURL = new URL(url);
        this.fileName = getFileNameFromURL(url);
        this.retryNumber = retryNumber;
    }

    public boolean download() {
        int tryNumber = 0;
        while (++tryNumber <= retryNumber) {
            System.out.println("[" + tryNumber + "/" + retryNumber + "] " + fileURL + "\n\t -> " + fileName + "...");
            var fileToDownload = new File("data/" + fileName);
            try {
                FileUtils.copyURLToFile(fileURL, fileToDownload);
            } catch (IOException e) {
                fileToDownload.setWritable(true);
                fileToDownload.delete();
                System.out.println("ERR: " + fileURL);
                continue;
            }
            finally {
                System.gc();
            }
            return true;
        }
        return false;
    }

    private String getFileNameFromURL(String fileUrl) {
        var splitted = fileUrl.split("/");
        return splitted[splitted.length - 1];
    }

    public String getFileName() {
        return fileName;
    }
}
