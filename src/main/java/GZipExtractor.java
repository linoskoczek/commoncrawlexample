import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class GZipExtractor {
    private String filePath;
    private String newFilePath;

    public GZipExtractor(String filePath) {
        this.filePath = filePath;
        newFilePath = convertGZipNameToPure(filePath);
    }

    public boolean extractFile() {
        GZIPInputStream gzipInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            gzipInputStream = new GZIPInputStream(new FileInputStream(filePath));
            fileOutputStream = new FileOutputStream(newFilePath);

            byte[] buf = new byte[1024];
            int len;
            while ((len = gzipInputStream.read(buf)) > 0) {
                fileOutputStream.write(buf, 0, len);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Extraction error (File not found): " + filePath);
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            System.err.println("Extraction error: " + filePath);
            e.printStackTrace();
            return false;
        }
        finally {
            if(gzipInputStream != null && fileOutputStream != null) {
                try {
                    gzipInputStream.close();
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.gc();
        }
        return true;
    }

    private String convertGZipNameToPure(String name) {
        return name.substring(0, name.length() - 3);
    }

    public String getNewFilePath() {
        return newFilePath;
    }
}
