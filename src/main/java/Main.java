import java.io.FileNotFoundException;

public class Main {
    public static final Statistics STATISTICS = Statistics.getInstance();

    private static boolean firstTime = true;
    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        var pathReader = new WETReader("data/wet.paths");

        String file;
        while(pathReader.hasNext() && (file = pathReader.next()) != null) {
            var thread = new Thread(new WARCFileProcessor(file, STATISTICS));
            runThread(thread, Integer.parseInt(args[0]));
        }
    }

    public static void runThread(Thread thread, int numOfThreads) throws InterruptedException {
        int half = numOfThreads /2;
        while(WARCFileProcessor.threadCounter >= numOfThreads) {
            Thread.sleep(500);
        }
        if(firstTime && WARCFileProcessor.threadCounter > half) {
            Thread.sleep(180000);
            firstTime = false;
        }
        thread.start();
        Thread.sleep(500);
    }
}
