import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;

public class Statistics {
    private static Statistics statistics;
    private static volatile BigInteger wholeLetterI = BigInteger.ZERO, wholeLetterU = BigInteger.ZERO;


    private Statistics() {
    }

    public static synchronized Statistics getInstance() {
        if (statistics == null)
            statistics = new Statistics();
        return statistics;
    }

    public void print() {
        var stats = "(I) " + wholeLetterI + " / (U) " + wholeLetterU;
        System.out.println("[STATISTICS] " + stats);
        try {
            var file = new File("data/results.txt");
            if (file.exists()) file.delete();
            FileUtils.writeStringToFile(file, stats, Charsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void addU(BigInteger letterU) {
        wholeLetterU = wholeLetterU.add(letterU);
    }

    public synchronized void addI(BigInteger letterI) {
        wholeLetterI = wholeLetterI.add(letterI);
    }
}
