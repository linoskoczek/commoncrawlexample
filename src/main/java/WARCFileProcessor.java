import org.apache.commons.io.IOUtils;

import java.io.File;
import java.math.BigInteger;
import java.net.MalformedURLException;

public class WARCFileProcessor implements Runnable {
    public static volatile int threadCounter = 0;

    private final Statistics statistics;
    private String fileUrl;
    private File downloadedArchive, downloadedExtracted;

    public WARCFileProcessor(String fileUrl, Statistics statistics) {
        this.fileUrl = fileUrl;
        this.statistics = statistics;
    }

    private static synchronized void increaseThreadCounter() {
        threadCounter++;
    }

    private static synchronized void decreaseThreadCounter() {
        threadCounter--;
    }

    @Override
    public void run() {
        increaseThreadCounter();
        try {
            var downloader = new FileDownloader(fileUrl, 3);
            downloadedArchive = new File("data/" + downloader.getFileName());
            downloader.download();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //var downloadedArchive = new File("data/CC-MAIN-20180918130631-20180918150631-00000.warc.wet.gz");
        if (downloadedArchive != null) {
            downloadedExtracted = extractFile(downloadedArchive);
            downloadedArchive.setWritable(true);
            downloadedArchive.delete();
            BigInteger[] results = readFile(downloadedExtracted);
            downloadedExtracted.setWritable(true);
            downloadedExtracted.delete();
            updateIUValues(results[0], results[1]);
            System.out.println("[OK] (I) " + results[0] + "/ (U) " + results[1]);
            statistics.print();
            System.gc();
        } else {
            System.out.println("[THREAD FINISHED WITH ERROR]");
        }

        decreaseThreadCounter();
    }

    private BigInteger[] readFile(File downloadedExtracted) {
        var reader = new WARCFileReader(downloadedExtracted);
        return reader.readFile();
    }

    private File extractFile(File downloadFile) {
        System.out.println("Extracting: " + downloadFile.getPath());
        var extractor = new GZipExtractor(downloadFile.getPath());
        extractor.extractFile();

        return new File(extractor.getNewFilePath());
    }

    private void updateIUValues(BigInteger letterI, BigInteger letterU) {
        statistics.addI(letterI);
        statistics.addU(letterU);
    }
}
