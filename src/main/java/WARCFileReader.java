import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

public class WARCFileReader {
    private File filePath;
    private volatile BigInteger letterI = BigInteger.ZERO, letterU = BigInteger.ZERO;

    public WARCFileReader(File filePath) {
        this.filePath = filePath;
    }

    public BigInteger[] readFile() {
        try {
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            int c;
            while ((c = in.read()) != -1) {
                countLetters(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new BigInteger[]{letterI, letterU};
    }

    private void countLetters(int c) {
        if (c == 'i' || c == 'I') {
            increaseI();
        } else if (c == 'u' || c == 'U') {
            increaseU();
        }
    }

    private synchronized void increaseU() {
        letterU = letterU.add(BigInteger.ONE);
    }

    private synchronized void increaseI() {
        letterI = letterI.add(BigInteger.ONE);
    }
}
