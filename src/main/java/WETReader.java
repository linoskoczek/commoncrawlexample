import java.io.FileNotFoundException;
import java.util.Iterator;

public class WETReader extends WebArchiveReader implements Iterator {
    public WETReader(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    @Override
    public String next() {
        return "https://commoncrawl.s3.amazonaws.com/" + scanner.nextLine();
    }
}
