import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public abstract class WebArchiveReader {
    Scanner scanner;

    public WebArchiveReader(String fileName) throws FileNotFoundException {
        this.scanner = new Scanner(new File(fileName));
    }

    public boolean hasNext() {
        return scanner.hasNextLine();
    }

    public abstract String next();
}
