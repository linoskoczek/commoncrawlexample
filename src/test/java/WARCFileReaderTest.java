import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;

public class WARCFileReaderTest {
    String content;
    File file;

    @Before
    public void prepareRandomFile() throws IOException {
        content = RandomStringGenerator.randomString(1000000);
        file = new File("test.txt");
        FileUtils.writeStringToFile(file, content, Charsets.UTF_8);
    }

    @After
    public void clean() {
        file.delete();
    }

    @Test
    public void checkIfCounterIsCorrect() {
        var resultFromJavaStringSmallI = new BigInteger(String.valueOf(occurrences(content, "i")));
        var resultFromJavaStringSmallU = new BigInteger(String.valueOf(occurrences(content, "u")));
        var resultFromJavaStringBigI = new BigInteger(String.valueOf(occurrences(content, "I")));
        var resultFromJavaStringBigU = new BigInteger(String.valueOf(occurrences(content, "U")));

        var resultFromJavaI = resultFromJavaStringSmallI.add(resultFromJavaStringBigI);
        var resultFromJavaU = resultFromJavaStringSmallU.add(resultFromJavaStringBigU);

        var resultFromImplementation = new WARCFileReader(file).readFile();

        Assert.assertEquals(resultFromJavaI, resultFromImplementation[0]);
        Assert.assertEquals(resultFromJavaU, resultFromImplementation[1]);
    }

    private int occurrences(String line, String txt) {
        return line.length() - line.replace(txt, "").length();
    }

}
